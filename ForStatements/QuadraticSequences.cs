﻿namespace ForStatements
{
    public static class QuadraticSequences
    {
        public static uint CountQuadraticSequenceTerms(long a, long b, long c, long maxTerm)
        {
            long term = 0;
            uint i = 1;

            for (; term < maxTerm; i++)
            {
                term = (a * i * i) + (b * i) + c;
            }

            if (i > 0)
            {
                return i - 1;
            }
            else
            {
                return 0;
            }
        }

        public static ulong GetQuadraticSequenceTermsProduct1(uint count)
        {
            uint term1 = 13;
            uint term2 = 38;
            ulong sum;
            if (count == 1)
            {
                sum = term1;
            }
            else if (count == 2)
            {
                sum = term1 * term2;
            }
            else
            {
                sum = term1 * term2;
                for (int i = 0; i < count - 2; i++)
                {
                    uint temp = (2 * term2) - term1 + 14;
                    term1 = term2;
                    term2 = temp;
                    sum *= temp;
                }
            }

            return sum;
        }

        public static ulong GetQuadraticSequenceProduct2(long a, long b, long c, long startN, long count)
        {
            ulong sum = 1;

            for (long i = 0; i < startN + count; i++)
            {
                if (i >= startN)
                {
                    sum *= (ulong)((a * i * i) + (b * i) + c);
                }
            }

            return sum;
        }
    }
}
