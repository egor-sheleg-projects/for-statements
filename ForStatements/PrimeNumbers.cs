﻿namespace ForStatements
{
    public static class PrimeNumbers
    {
        public static bool IsPrimeNumber(uint n)
        {
            int a = 0;
            for (int i = 1; i <= n; i++)
            {
                if (n % i == 0)
                {
                    a++;
                }
            }

            if (a == 2)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static ulong SumDigitsOfPrimeNumbers(int start, int end)
        {
            ulong sum = 0;

            for (int i = start; i <= end; i++)
            {
                int n = i;
                int a = 0;
                int j = 1;
                for (; j <= n; j++)
                {
                    if (n % j == 0)
                    {
                        a++;
                    }

                    if (a > 2)
                    {
                        break;
                    }
                }

                if (a == 2 && n > 10)
                {
                    sum += (ulong)(((n - (n % 10)) / 10) + (n % 10));
                }
                else if (a == 2 && n < 10)
                {
                    sum += (ulong)n;
                }
            }

            return sum;
        }
    }
}
