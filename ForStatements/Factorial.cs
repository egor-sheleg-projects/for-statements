﻿namespace ForStatements
{
    public static class Factorial
    {
        public static int GetFactorial(int n)
        {
            int sum = 1;

            for (int i = 1; i <= n; i++)
            {
                sum *= i;
            }

            return sum;
        }

        public static int SumFactorialDigits(int n)
        {
            if (n == 0)
            {
                return 1;
            }
            else
            {
                int product = 1;

                for (int i = 1; i <= n; i++)
                {
                    product *= i;
                }

                int sum = 0;

                for (; product > 0; product /= 10)
                {
                    sum += product % 10;
                }

                return sum;
            }
        }
    }
}
