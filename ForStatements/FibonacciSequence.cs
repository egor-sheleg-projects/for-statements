﻿namespace ForStatements
{
    public static class FibonacciSequence
    {
        public static int GetFibonacciNumber(int n)
        {
            int sum = 0;
            int first = 0, second = 1;
            if (n == 0)
            {
                return 0;
            }
            else if (n == 1)
            {
                return 1;
            }
            else
            {
                for (int i = 2; i <= n; i++)
                {
                    int temp = first + second;
                    if (i == n)
                    {
                        sum += temp;
                    }

                    first = second;
                    second = temp;
                }
            }

            return sum;
        }

        public static ulong GetProductOfFibonacciNumberDigits(ulong n)
        {
            ulong sum = 0;
            ulong first = 0, second = 1;
            if (n == 0)
            {
                return 0;
            }
            else if (n == 1)
            {
                return 1;
            }
            else
            {
                for (ulong i = 2; i <= n; i++)
                {
                    ulong temp = first + second;
                    if (i == n && temp < 10)
                    {
                        sum = temp;
                    }
                    else if (i == n && temp >= 10 && temp < 100)
                    {
                        sum = (temp - (temp % 10)) / 10 * (temp % 10);
                    }
                    else if (i == n && temp >= 100)
                    {
                        sum = (temp - (temp % 10)) / 100 * ((temp % 100) - (temp % 10)) / 10 * (temp % 10);
                    }

                    first = second;
                    second = temp;
                }
            }

            return sum;
        }
    }
}
